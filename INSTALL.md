Prerequisites
-------------

You will need to have these libraries in order to build Gappa:
  - GMP    https://gmplib.org/
  - MPFR   https://www.mpfr.org/
  - Boost  https://www.boost.org/

Note that the Boost libraries do not have to be built, as Gappa depends
only on library headers.

You will also need to have Flex and Bison.

The `.tar.gz` file is distributed with a working set of configure files. They
are not in the git repository though. Consequently, if you are building from
git, you will need autoconf (>= 2.70).


Configuring, compiling, and installing
--------------------------------------

Ideally, you should just have to type:

    ./configure && ./remake

A few environment variables can be passed to the `./configure` script if
it failed. `CPPFLAGS` and `LDFLAGS` can be used to locate the libraries
if they are not in standard locations. `CXX` and `CXXFLAGS` define the
compiler and its compilation flags. Bloated example:

    ./configure "CXX=distcc g++" "CXXFLAGS=-O3" "CPPFLAGS=-I/usr/local/include" "LDFLAGS=-L/usr/local/lib"

If everything went well, the script should say it created `Remakefile`.
You can then run `./remake` to compile Gappa. As for the installation,
none is needed, as `src/gappa` is a standalone executable file. But if
you still want to install it, `./remake install` will do the job. The
installation location is governed by standard `./configure` settings. You
can always run on `./configure --help` for additional information.

As a last tip, you can use `./config.status -V` if you have already
compiled a previous version of Gappa. The output of this script will give
you the options you used the last time you ran `./configure` in the
current directory.


Documentation
-------------

Running `./remake doc/html/index.html` generates the HTML documentation.
You need a working installation of Sphinx to do so.
