# Binary64 rounding operator:
@binary64 = float<ieee_64,ne>;

# Definition of the polynomial coefficients
# These are exact floating point values
log_p_coeffs_c1 = 0x1p0;
log_p_coeffs_c2 = -0x1p-1;
log_p_coeffs_c3 = 0x1.5555555555557p-2;
log_p_coeffs_c4 = -0x1.000000000001dp-2;
log_p_coeffs_c5 = 0x1.9999999957453p-3;
log_p_coeffs_c6 = -0x1.5555554bcc9e5p-3;
log_p_coeffs_c7 = 0x1.2492622a42396p-3;
log_p_coeffs_c8 = -0x1.00054b3614f8p-3;
log_p_coeffs_c9 = 0x1.c12fe341830efp-4;

# Explain to Gappa where the reduced argument is
# coming from
y = binary64(Vc);
m = binary64(Vm);
r = y * m - 1;

# Definition of rhi and rlo
rhi = binary64(r);
rlo = -(rhi - r);

#Definition of mathematically exact polynomial
Mq = r * (log_p_coeffs_c3 + r * (log_p_coeffs_c4 + r * (log_p_coeffs_c5 + r * (log_p_coeffs_c6 + r * (log_p_coeffs_c7 + r * (log_p_coeffs_c8 + r * log_p_coeffs_c9))))));
Mv = log_p_coeffs_c2 + Mq;
Mw = r * Mv;
Mu = log_p_coeffs_c1 + Mw;
Mp = r * Mu;

#Translation of C code to Gappa
q7 = log_p_coeffs_c9;
q6 = binary64(log_p_coeffs_c8 + rhi * q7);
q5 = binary64(log_p_coeffs_c7 + rhi * q6);
q4 = binary64(log_p_coeffs_c6 + rhi * q5);
q3 = binary64(log_p_coeffs_c5 + rhi * q4);
q2 = binary64(log_p_coeffs_c4 + rhi * q3);
q1 = binary64(log_p_coeffs_c3 + rhi * q2);
q = binary64(rhi * q1);

# vhi + vlo ~= log_p_coeffs_c2 + q
v = log_p_coeffs_c2 + q;
vhi = binary64(v);
vlo = -(vhi - v);

# Renamed for ease of proof
e = rhi * vhi;
eh = binary64(e);
el = -(eh - e);

f = rhi * vlo;
fh = binary64(f);

g = rlo * vhi;
gh = binary64(g);

i = rlo * vlo;

w1 = binary64(fh + gh);
w2 = binary64(w1 + el);

w = eh + w2;
whi = binary64(w);
wlo = -(whi - w);

# uhi + ulo ~= (log_p_coeffs_c1 + whi) + wlo
u_ut = log_p_coeffs_c1 + whi;
uthi = binary64(u_ut);
uttlo = -(uthi - u_ut);
utlo = binary64(uttlo + wlo);
u = uthi + utlo;
uhi = binary64(u);
ulo = -(uhi - u);

# Renamed for ease of proof
a = rhi * uhi;
ah = binary64(a);
al = -(ah - a);

b = rhi * ulo;
bh = binary64(b);

c = rlo * uhi;
ch = binary64(c);

p1 = binary64(bh + ch);
p2 = binary64(p1 + al);

p = ah + p2;
ph = binary64(p);
pl = -(ph - p);

# Implication to prove
{(
   r in [-0x1.f81f81f81f85fff50538ddbd8p-8,0x1.6689da279dfp-7]
/\ r <> 0
)
->
(
       q -/ Mq in ? /\
       v -/ Mv in ? /\
       w -/ Mw in ? /\
       u -/ Mu in ? /\
       |p -/ Mp| <= 1b-66
)}

# Hints
u - (log_p_coeffs_c1 + w) -> utlo - (uttlo + wlo);
p - r * u -> (p1 - (b + c)) + (p2 - (p1 + al)) - rlo * ulo;
w - r * v -> (w1 - (f + g)) + (w2 - (w1 + el)) - rlo * vlo;
